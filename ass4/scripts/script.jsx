function setup() {
    // render the form
    ReactDOM.render(<Weather />, document.querySelector('#form'))
}

const APIKEY = "60aa18c314afbc257c4be4d406131892";

class Weather extends React.Component{
    constructor(props){
        super(props)
        this.state ={
            city: undefined,
            icon: undefined,
            temp:undefined,
            description: "",
            showError: false,
            showResult: false,
            errorMsg: ""
        }
        this.apiURL = "api.openweathermap.org/data/2.5/weather?q=cityname&appid=API key";
        this.handleSumit = this.submit.bind(this);
        
    } 

submit(e){
    e.preventDefault()
    console.log("get data for" + this.state.city)
    this.fetchWeather()
}

fetchWeather() {
    let url = new URl (this.apiURL)
    url.searchParams.set("q", this.state.city)
    url.searchParams.set("appid", APIKEY)
    console.log(url)
    fetch(url)
        .then(function (response) {
            if (response.ok) {
                return response.json();
            }
            else {
                throw new Error("Status code of error is " + response.status + " Please Try Again.")
            }
        })
        .then(json => this.dataRender(json))
        .catch(error => this.errorRender(error, url));
}

errorRender(e, url){
    console.log( "Error" +e.message + " ")
    console.log("url" + url)
    this.setState ({showError: true, showResult:false, errorMsg: e.message})
}

dataRender(json){
console.log(json)
this.setState({showError:false, showResult: true})
}




render(){
    return(
        <div>
        <form onSubmit={this.handleSubmit}>
                <input type="text" required value={this.state.temp}
                    onChange={this.handleChange} />
                <br />
                <label htmlFor="long">Longitude</label>
                <input type="number" step=".00001" id="long" name="long"
                    required value={this.state.long}
                    onChange={this.handleChange} />
            <input type="submit" id="submit" value="Get the times"></input>
        </form>
        <div id='result' style={ styleResult } >
            <p >Sunrise: {this.state.sunrise} </p>
            <p >Sunset: {this.state.sunset} </p>
        </div>
        <div id='error' style={ styleError }>
            <p >Error: {this.state.errorMsg} </p>
        </div>
    </div>
        </div>



    )
}


}